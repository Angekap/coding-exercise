# README #

This README documents is for the coding exercise.

### What is this repository for? ###

This a fork of the coding exercise.

The exercise is implemented using the following:

* HTML5
* CSS3
* JavaScript
* [EJS](http://www.embeddedjs.com)
* [LESS](http://lesscss.org)
* [Node.js](https://nodejs.org/en/)
* [Grunt.js](https://gruntjs.com/)
* [Express.js](https://expressjs.com)
* [Bootstrap](http://getbootstrap.com/)

### Notes ###

The code logic are spread across 2 files **_jsDefaulf_** (located in views/template) and ** _app.js_**.
I've also provided the **_tests.js_** file. Here I've refactored the provided solution (found in **_app.js_**) using a Functional approach.

Unfortunately this solution is not used because of a blocker I encountered (see comments in **_tests.js_** for more details)

### How do I get set up? ###

To run and view the completed exercise, complete the following steps:

#### Step 1 ####

Make sure you have the following installed:

* [Git](https://git-scm.com)
* [Node.js](https://nodejs.org/en/)
* [Sublime](https://www.sublimetext.com) | [Atom](https://atom.io) | [Notepadd++](https://notepad-plus-plus.org) - choose one text editor.

Clone the repo locally.

#### Step 2 ####

Run the below commands on your terminal:

* Run `npm install`
* Run `grunt dev`
* Run `npm start`
* Visit http://localhost:5000/


### Who do I talk to? ###

* Repo owner - [Ange Lubiba-Tshibenji](https://bitbucket.org/Angekap/)
