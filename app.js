var express = require('express');
var bodyParser = require('body-parser');
var reload = require('reload');
var fs = require('fs');
var app = express();


//Listening to available port from environment variable
//If no environment variable is set, use port 5000 as default
app.set('port', process.env.PORT || 5000);

/*
  Setup viewing engine - ejs (embedddable Javascript)
*/
app.set('view engine', 'ejs');
app.set('views', 'views'); //setup views location

/*
  Create a global variable that can is available to any of my routes (pages)
*/
app.locals.siteTitle = 'Gamesys Promo Test ';
// app.locals.allSpeakers = featureFile.speakers;

/*
  Express middleware - express.static:
  method used to setup a folder for holding files that I want the use or routes to have access to.
  This is accessible through the root.
*/
app.use(express.static('public/assets'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

/*
  It is not practical to have all routes in a single file. Thus creating separate routes (modularizing my routes) is more advantageous.
*/
app.use(require('./routes/index'));

// location of local json file
var url = './public/assets/data/emails.json';

// create a new subscription object
var subscription = {
  subscriptions: []
};
// convert it to json
var json = JSON.stringify(subscription);

// add new json object to local file
writeToFile(url, json);

/*
  get email data from AJAX 'POST' request.
  Read existing file & check whether it contains the email:
  -if true, don't add it, respond with an error message.
  -else false, add the new email to json file.
*/
app.post('/', function(req, resp) {
  newEmail = req.body.email;
  fs.readFile(url, function(err, data) {
    if (containsEmail(subscription.subscriptions, 'email', newEmail)) {
      resp.json("Failed! Email already exist!");
    } else {
      subscription = JSON.parse(data);
      subscription.subscriptions.push({email: newEmail});
      json = JSON.stringify(subscription);
      addEmailToFile(url, json, resp);
      console.log(subscription.subscriptions);
    }
  });
});

// function to add email to file
function addEmailToFile(url, data, resp) {
  writeToFile(url, data);
  resp.json("Success");
}

// function to save json object to local file
function writeToFile(url, data) {
  fs.writeFile(url, data, function(err) {
    if (err) {
      throw err;
    }
  });
}

// check if email exist already in array
function containsEmail(arr, prop, val) {
  if (arr.length >= 0 ) {
    for (var i in arr) {
      if (arr[i][prop] === val) {
        return true;
      }
    }
  }
  return false;
}

var server = app.listen(app.get('port'), function(){
  console.log('Listening on port ' + app.get('port'));
});

reload(server, app);
